package com.gc.log;

import lombok.extern.log4j.Log4j2;

import java.util.HashMap;
import java.util.Map;

@Log4j2
public class RunApp {
    public static void main(String[] args) {
        log.info("----------------------------------------------------------------------");
        log.info("Create people.");
        Map<String, Person> people = new HashMap();
        people.put("Engineer", new Person("Mike", 34));
        people.put("Important", new Person("Tom", 22));
        people.put("Friend", new Person("Criss", 34));
        people.put("Teacher", new Person("Stefan", 52));
        log.info("People were created: {}", people);
        log.info("Amount of people: {}", people.size());

        people.remove("Friend");
        people.remove("Engineer");

        System.gc();
        log.info("GC was run.");
        try {
            log.info("Wait for 3 seconds...");
            Thread.sleep(3_000);
            System.gc();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        System.gc();

        people.put("Engineer", new Person("Mike", 34));
        log.info("People were updated: {}", people);
        log.info("Amount of people: {}", people.size());
    }
}
